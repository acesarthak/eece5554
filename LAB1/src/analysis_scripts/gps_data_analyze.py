import rosbag
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from statistics import stdev, mean
import numpy as np
import sys
import utm
import math


def dist_from_mean(centroid, x_values, y_values):
    shortest_dist = []
    for i in range(len(x_values)):
        x = x_values[i]
        y = y_values[i]
        dist = math.sqrt((x - centroid[0])**2 + (y - centroid[1])**2)
        shortest_dist.append(dist)
        shortest_dist_square = [number**2 for number in shortest_dist]
    return mean(shortest_dist), min(shortest_dist), max(shortest_dist), mean(shortest_dist_square)

# Function to find distance


def shortest_distance(x1, y1, a, b, c):

    d = abs((a * x1 + b * y1 + c)) / (math.sqrt(a * a + b * b))
    return d


def absolute_error(slope, intercept, x_values, y_values):
    shortest_dist = []
    for i in range(len(x_values)):
        x = x_values[i]
        y = y_values[i]
        dist = shortest_distance(x, y, -1.0*slope, 1, -1.0*intercept)
        shortest_dist.append(dist)
        shortest_dist_square = [number**2 for number in shortest_dist]
    return mean(shortest_dist), min(shortest_dist), max(shortest_dist), mean(shortest_dist_square)


if len(sys.argv) == 5:
    # Read stationary gps data bag
    bag_stationary = rosbag.Bag(
        '../data/bag_files/' + str(sys.argv[1]), 'r')
    # Read moving gps data bag
    bag_moving = rosbag.Bag(
        '../data/bag_files/' + str(sys.argv[2]), 'r')
    # Read actual gps location where stationary data is collected in degrees
    lat_actual_stationary = float(sys.argv[3])
    lon_actual_stationary = float(sys.argv[4])
else:
    bag_stationary = rosbag.Bag(
        '../data/bag_files/gps_data_stationary.bag', 'r')
    bag_moving = rosbag.Bag(
        '../data/bag_files/gps_data_moving.bag', 'r')
    lat_actual_stationary = 42.33291
    lon_actual_stationary = -71.10947

print("Stationary GPS Data")
utm_easting = []
utm_northing = []
altitude = []

for topic, msg, t in bag_stationary.read_messages(topics=['/gps_data']):
    utm_easting.append(msg.utm_easting)
    utm_northing.append(msg.utm_northing)
    altitude.append(msg.altitude)

bag_stationary.close()

mean_stationary = (mean(utm_easting), mean(utm_northing))
utm_val = utm.from_latlon(lat_actual_stationary, lon_actual_stationary)
actual_stationary = (utm_val[0], utm_val[1])
mean_absolute_error, min_error, max_error, mean_squared_error = dist_from_mean(
    mean_stationary, utm_easting, utm_northing)

print("Max deviation from mean for any GPS data point: ", max_error)
print("Min deviation from mean for any GPS data point: ", min_error)
print("Mean Absolute error for GPS data points from mean: ",
      mean_absolute_error)
print("Mean Squared error for GPS data points from mean: ", mean_squared_error)
print("Mean location of stationary data using GPS sensor: ", mean_stationary)
print("Actual location of stationary data from Google Maps: ", actual_stationary)
print("Error between mean gps sensor location and actual location: ",
      (mean_stationary[0] - actual_stationary[0], mean_stationary[1] - actual_stationary[1]))
print("Standard deviation of stationary data: ",
      (stdev(utm_easting), stdev(utm_northing)))
print("Altitude Range: ", (min(altitude), max(altitude)))
print("Mean of Altitude: ", mean(altitude))

# Stationary 2D plot
plt.scatter(utm_easting, utm_northing, s=4, c='g')
plt.xlabel('utm_easting (m)')
plt.ylabel('utm_northing (m)')
plt.title('Stationary GPS Data in 2D')
plt.scatter(mean_stationary[0], mean_stationary[1], s=8, c='r')
plt.scatter(actual_stationary[0], actual_stationary[1], s=8, c='b')
plt.legend(["Gps sensor data points", "Mean of gps data", "Actual Location"])
plt.show()

# Stationary 3D plot
ax = plt.axes(projection='3d')
ax.set_xlabel('utm_easting (m)')
ax.set_ylabel('utm_northing (m)')
ax.set_zlabel('altitude (m)')
ax.set_title('Stationary GPS Data in 3D')
ax.scatter3D(utm_easting, utm_northing, altitude, c=altitude, cmap='Greens')
plt.show()


print()
print("Moving GPS data")
utm_easting = []
utm_northing = []
altitude = []

for topic, msg, t in bag_moving.read_messages(topics=['/gps_data']):
    utm_easting.append(msg.utm_easting)
    utm_northing.append(msg.utm_northing)
    altitude.append(msg.altitude)


bag_moving.close()


# find line of best fit
a, b = np.polyfit(np.array(utm_easting), np.array(utm_northing), 1)
mean_absolute_error, min_error, max_error, mean_squared_error = absolute_error(
    a, b, utm_easting, utm_northing)

print("Max deviation from best fit line for any GPS data point: ", max_error)
print("Min deviation from best fit line for any GPS data point: ", min_error)
print("Mean Absolute error for GPS data points from best fit line: ",
      mean_absolute_error)
print("Mean Squared error for GPS data points from best fit line: ", mean_squared_error)
print("Altitude Range: ", (min(altitude), max(altitude)))
print("Mean of Altitude: ", mean(altitude))


# Moving 2D plot
# add points to plot
plt.scatter(utm_easting, utm_northing, s=2, c='g')
plt.xlabel('utm_easting (m)')
plt.ylabel('utm_northing (m)')
plt.title('Moving GPS Data in 2D with best fit line')

# add line of best fit to plot
plt.plot(utm_easting, a*np.array(utm_easting) + b)
plt.legend(["Best Fit Line", "Gps sensor data points"])
plt.show()


# Moving 3D plot
ax = plt.axes(projection='3d')
ax.set_xlabel('utm_easting (m)')
ax.set_ylabel('utm_northing (m)')
ax.set_zlabel('altitude (m)')
ax.set_title('Moving GPS Data in 3D')
ax.scatter3D(utm_easting, utm_northing, altitude, c=altitude, cmap='Greens')
plt.show()
