% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 4371.550584513203830 ; 4366.438948010577406 ];

%-- Principal point:
cc = [ 1566.100048613106537 ; 2019.981873076699458 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.170512930244329 ; -0.883407018192082 ; -0.007541180767464 ; 0.001939293817170 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 11.990633173108030 ; 11.630047032840681 ];

%-- Principal point uncertainty:
cc_error = [ 15.092181119519303 ; 19.601478899585882 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.020634455775414 ; 0.149808325557647 ; 0.001833734337343 ; 0.001421377038312 ; 0.000000000000000 ];

%-- Image size:
nx = 3072;
ny = 4096;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 25;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.206036e+00 ; 2.126154e+00 ; 1.185167e+00 ];
Tc_1  = [ 1.492812e+02 ; -3.205105e+02 ; 1.541231e+03 ];
omc_error_1 = [ 3.884185e-03 ; 3.516256e-03 ; 5.932639e-03 ];
Tc_error_1  = [ 5.351031e+00 ; 6.956732e+00 ; 4.906651e+00 ];

%-- Image #2:
omc_2 = [ 1.497920e+00 ; 2.272228e+00 ; -9.746946e-01 ];
Tc_2  = [ -5.041188e+01 ; -3.408289e+02 ; 2.040430e+03 ];
omc_error_2 = [ 2.607848e-03 ; 4.805952e-03 ; 6.571281e-03 ];
Tc_error_2  = [ 7.024723e+00 ; 9.191493e+00 ; 4.941068e+00 ];

%-- Image #3:
omc_3 = [ -1.226362e+00 ; -1.637292e+00 ; 3.254311e-01 ];
Tc_3  = [ -2.732136e+02 ; -4.857606e+02 ; 1.619528e+03 ];
omc_error_3 = [ 4.038912e-03 ; 3.132610e-03 ; 4.609957e-03 ];
Tc_error_3  = [ 5.647288e+00 ; 7.301710e+00 ; 5.111586e+00 ];

%-- Image #4:
omc_4 = [ -9.831679e-01 ; -1.763902e+00 ; 4.320009e-01 ];
Tc_4  = [ -1.022674e+02 ; -4.257533e+02 ; 1.478329e+03 ];
omc_error_4 = [ 3.668858e-03 ; 3.312709e-03 ; 4.622105e-03 ];
Tc_error_4  = [ 5.123182e+00 ; 6.632641e+00 ; 4.474459e+00 ];

%-- Image #5:
omc_5 = [ -2.210444e+00 ; -2.163366e+00 ; 1.264976e-01 ];
Tc_5  = [ -2.857359e+02 ; -2.620494e+02 ; 1.269423e+03 ];
omc_error_5 = [ 4.363750e-03 ; 4.158190e-03 ; 9.169075e-03 ];
Tc_error_5  = [ 4.374743e+00 ; 5.743672e+00 ; 3.973060e+00 ];

%-- Image #6:
omc_6 = [ 1.999959e+00 ; 1.744609e+00 ; 1.246998e+00 ];
Tc_6  = [ -1.039400e+02 ; -2.610172e+02 ; 1.121252e+03 ];
omc_error_6 = [ 4.322786e-03 ; 2.747446e-03 ; 6.135094e-03 ];
Tc_error_6  = [ 3.913696e+00 ; 5.063358e+00 ; 4.108867e+00 ];

%-- Image #7:
omc_7 = [ -2.146394e+00 ; -2.022675e+00 ; -8.305822e-01 ];
Tc_7  = [ -2.348489e+02 ; -2.199631e+02 ; 1.109413e+03 ];
omc_error_7 = [ 2.976541e-03 ; 4.426902e-03 ; 6.520758e-03 ];
Tc_error_7  = [ 3.839522e+00 ; 5.069672e+00 ; 3.899348e+00 ];

%-- Image #8:
omc_8 = [ -1.741806e+00 ; -1.915115e+00 ; -1.531174e-01 ];
Tc_8  = [ -2.562225e+02 ; -4.142962e+02 ; 1.341141e+03 ];
omc_error_8 = [ 3.776429e-03 ; 3.448744e-03 ; 6.026492e-03 ];
Tc_error_8  = [ 4.677026e+00 ; 6.121718e+00 ; 4.471688e+00 ];

%-- Image #9:
omc_9 = [ -1.473849e+00 ; -1.829507e+00 ; 2.924142e-01 ];
Tc_9  = [ -2.176651e+02 ; -3.671756e+02 ; 1.355838e+03 ];
omc_error_9 = [ 3.688573e-03 ; 3.065400e-03 ; 5.234771e-03 ];
Tc_error_9  = [ 4.680919e+00 ; 6.106067e+00 ; 3.969846e+00 ];

%-- Image #10:
omc_10 = [ -1.238344e+00 ; -1.737708e+00 ; 4.671790e-01 ];
Tc_10  = [ -2.398886e+02 ; -4.784184e+02 ; 1.493704e+03 ];
omc_error_10 = [ 3.957351e-03 ; 3.065490e-03 ; 4.824838e-03 ];
Tc_error_10  = [ 5.212306e+00 ; 6.732039e+00 ; 4.601987e+00 ];

%-- Image #11:
omc_11 = [ 2.046157e+00 ; 1.820784e+00 ; 9.147244e-01 ];
Tc_11  = [ 4.349559e+01 ; -2.817367e+02 ; 1.391888e+03 ];
omc_error_11 = [ 4.459289e-03 ; 2.755023e-03 ; 6.631393e-03 ];
Tc_error_11  = [ 4.875482e+00 ; 6.218289e+00 ; 4.766479e+00 ];

%-- Image #12:
omc_12 = [ 2.223674e+00 ; 2.118779e+00 ; 6.786425e-01 ];
Tc_12  = [ -1.395704e+02 ; -3.118010e+02 ; 1.254708e+03 ];
omc_error_12 = [ 4.259308e-03 ; 2.936510e-03 ; 7.753247e-03 ];
Tc_error_12  = [ 4.383415e+00 ; 5.657663e+00 ; 4.374640e+00 ];

%-- Image #13:
omc_13 = [ -1.918530e+00 ; -1.973964e+00 ; -4.248610e-02 ];
Tc_13  = [ -3.068571e+02 ; -3.219663e+02 ; 1.383192e+03 ];
omc_error_13 = [ 3.965452e-03 ; 3.503853e-03 ; 6.807704e-03 ];
Tc_error_13  = [ 4.779039e+00 ; 6.275520e+00 ; 4.421848e+00 ];

%-- Image #14:
omc_14 = [ -1.533266e+00 ; -1.763027e+00 ; 4.671773e-01 ];
Tc_14  = [ -2.224029e+02 ; -4.044626e+02 ; 1.551065e+03 ];
omc_error_14 = [ 3.921094e-03 ; 2.926477e-03 ; 5.239287e-03 ];
Tc_error_14  = [ 5.368541e+00 ; 6.971844e+00 ; 4.392602e+00 ];

%-- Image #15:
omc_15 = [ -1.338307e+00 ; -1.603063e+00 ; 6.984873e-01 ];
Tc_15  = [ -2.066597e+02 ; -4.381625e+02 ; 1.690527e+03 ];
omc_error_15 = [ 4.087433e-03 ; 3.057003e-03 ; 4.686861e-03 ];
Tc_error_15  = [ 5.866546e+00 ; 7.594731e+00 ; 4.654173e+00 ];

%-- Image #16:
omc_16 = [ NaN ; NaN ; NaN ];
Tc_16  = [ NaN ; NaN ; NaN ];
omc_error_16 = [ NaN ; NaN ; NaN ];
Tc_error_16  = [ NaN ; NaN ; NaN ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ NaN ; NaN ; NaN ];
Tc_error_19  = [ NaN ; NaN ; NaN ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

%-- Image #21:
omc_21 = [ 1.831983e+00 ; 1.760321e+00 ; 2.405350e-01 ];
Tc_21  = [ -1.723020e+02 ; -4.237246e+02 ; 1.346611e+03 ];
omc_error_21 = [ 3.776020e-03 ; 2.754833e-03 ; 6.014527e-03 ];
Tc_error_21  = [ 4.701745e+00 ; 6.045003e+00 ; 4.165741e+00 ];

%-- Image #22:
omc_22 = [ 2.052672e+00 ; 1.954573e+00 ; -2.341328e-01 ];
Tc_22  = [ -2.574887e+02 ; -3.421220e+02 ; 1.456565e+03 ];
omc_error_22 = [ 3.753140e-03 ; 3.468504e-03 ; 7.144084e-03 ];
Tc_error_22  = [ 5.035264e+00 ; 6.541694e+00 ; 4.042457e+00 ];

%-- Image #23:
omc_23 = [ -2.031234e+00 ; -1.915148e+00 ; 7.369084e-01 ];
Tc_23  = [ -2.519623e+02 ; -2.980044e+02 ; 1.551189e+03 ];
omc_error_23 = [ 4.063777e-03 ; 2.623094e-03 ; 6.447549e-03 ];
Tc_error_23  = [ 5.326450e+00 ; 6.975768e+00 ; 3.878607e+00 ];

%-- Image #24:
omc_24 = [ -1.566564e+00 ; -1.761621e+00 ; 8.117056e-01 ];
Tc_24  = [ -2.586073e+02 ; -3.749447e+02 ; 1.452254e+03 ];
omc_error_24 = [ 4.046560e-03 ; 2.795446e-03 ; 5.259438e-03 ];
Tc_error_24  = [ 5.028606e+00 ; 6.577489e+00 ; 3.730938e+00 ];

%-- Image #25:
omc_25 = [ -1.298640e+00 ; -1.798077e+00 ; 9.350979e-01 ];
Tc_25  = [ -1.784990e+02 ; -4.354072e+02 ; 1.577834e+03 ];
omc_error_25 = [ 3.991377e-03 ; 3.085779e-03 ; 5.049781e-03 ];
Tc_error_25  = [ 5.481089e+00 ; 7.112661e+00 ; 4.089823e+00 ];

