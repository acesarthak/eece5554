# LAB3

## Perform the following steps to build your workspace

* `catkin_make`
  
## To collect imu_data (remember to source the workspace)

### Record rosbag

* `cd src/data/bag_files`
* `rosbag record /imu_data -O name_of_your_file.bag`
* `rosbag record /magnetometer_data -O name_of_your_file.bag`

### In a new terminal run following command to start the data driver

* `rosrun imu_driver imu_data.py`

## To analyze the data first go to root of workspace i.e. lab1 folder then run following commands

* `cd src/imu_driver/analysis`
### Open Matlab and run file allan_variance.mlx