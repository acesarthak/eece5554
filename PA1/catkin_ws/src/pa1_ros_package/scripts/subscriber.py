#!/usr/bin/env python

import rospy
from math import fabs
from std_msgs.msg import Float64
from geometry_msgs.msg import Pose
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState

pub_to_topic = rospy.Publisher(
    '/float_pub_topic', Float64, queue_size=1)


def transform_callback(data):
    print("Data received from topic /float_pub_topic = ", data)


def main():
    rospy.init_node('subscriber', anonymous=True)
    rospy.Subscriber('/float_pub_topic', Float64, transform_callback)
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
