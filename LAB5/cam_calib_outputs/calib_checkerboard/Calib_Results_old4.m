% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 4390.728544171245630 ; 4386.922798130172851 ];

%-- Principal point:
cc = [ 1566.529893450658619 ; 2012.044919024892579 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.176701020667095 ; -0.935921091629614 ; -0.007917932574929 ; 0.001881739374893 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 1.990185975721050 ; 1.932353843141403 ];

%-- Principal point uncertainty:
cc_error = [ 2.484587608799261 ; 3.210964976637986 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.003412116515370 ; 0.025020830745824 ; 0.000299358151401 ; 0.000233617229083 ; 0.000000000000000 ];

%-- Image size:
nx = 3072;
ny = 4096;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 25;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.204996e+00 ; 2.125222e+00 ; 1.185531e+00 ];
Tc_1  = [ 1.494680e+02 ; -3.174820e+02 ; 1.549085e+03 ];
omc_error_1 = [ 6.364933e-04 ; 5.761537e-04 ; 9.682834e-04 ];
Tc_error_1  = [ 8.812242e-01 ; 1.139900e+00 ; 8.128607e-01 ];

%-- Image #2:
omc_2 = [ 1.496264e+00 ; 2.268949e+00 ; -9.737919e-01 ];
Tc_2  = [ -5.122020e+01 ; -3.371026e+02 ; 2.047516e+03 ];
omc_error_2 = [ 4.280955e-04 ; 7.850424e-04 ; 1.074434e-03 ];
Tc_error_2  = [ 1.154972e+00 ; 1.503803e+00 ; 8.180461e-01 ];

%-- Image #3:
omc_3 = [ -1.228422e+00 ; -1.636836e+00 ; 3.255802e-01 ];
Tc_3  = [ -2.738761e+02 ; -4.826888e+02 ; 1.627306e+03 ];
omc_error_3 = [ 6.595536e-04 ; 5.141586e-04 ; 7.535882e-04 ];
Tc_error_3  = [ 9.294678e-01 ; 1.196073e+00 ; 8.443162e-01 ];

%-- Image #4:
omc_4 = [ -9.854719e-01 ; -1.763865e+00 ; 4.313898e-01 ];
Tc_4  = [ -1.030209e+02 ; -4.230443e+02 ; 1.485455e+03 ];
omc_error_4 = [ 5.988082e-04 ; 5.435665e-04 ; 7.549337e-04 ];
Tc_error_4  = [ 8.432756e-01 ; 1.086508e+00 ; 7.397418e-01 ];

%-- Image #5:
omc_5 = [ -2.210571e+00 ; -2.163171e+00 ; 1.265555e-01 ];
Tc_5  = [ -2.859011e+02 ; -2.597842e+02 ; 1.274735e+03 ];
omc_error_5 = [ 7.186991e-04 ; 6.869767e-04 ; 1.509829e-03 ];
Tc_error_5  = [ 7.198402e-01 ; 9.402402e-01 ; 6.574659e-01 ];

%-- Image #6:
omc_6 = [ 1.998793e+00 ; 1.745299e+00 ; 1.245816e+00 ];
Tc_6  = [ -1.039928e+02 ; -2.590106e+02 ; 1.128698e+03 ];
omc_error_6 = [ 7.077930e-04 ; 4.498952e-04 ; 1.003220e-03 ];
Tc_error_6  = [ 6.453229e-01 ; 8.308316e-01 ; 6.810880e-01 ];

%-- Image #7:
omc_7 = [ -2.146475e+00 ; -2.022529e+00 ; -8.303968e-01 ];
Tc_7  = [ -2.350383e+02 ; -2.175404e+02 ; 1.115762e+03 ];
omc_error_7 = [ 4.877249e-04 ; 7.238920e-04 ; 1.070361e-03 ];
Tc_error_7  = [ 6.323794e-01 ; 8.309485e-01 ; 6.465090e-01 ];

%-- Image #8:
omc_8 = [ -1.742420e+00 ; -1.915665e+00 ; -1.527457e-01 ];
Tc_8  = [ -2.562125e+02 ; -4.118355e+02 ; 1.347936e+03 ];
omc_error_8 = [ 6.175677e-04 ; 5.660777e-04 ; 9.889214e-04 ];
Tc_error_8  = [ 7.699672e-01 ; 1.002805e+00 ; 7.395080e-01 ];

%-- Image #9:
omc_9 = [ -1.474750e+00 ; -1.829252e+00 ; 2.929596e-01 ];
Tc_9  = [ -2.179153e+02 ; -3.646321e+02 ; 1.362271e+03 ];
omc_error_9 = [ 6.023914e-04 ; 5.034345e-04 ; 8.560318e-04 ];
Tc_error_9  = [ 7.704783e-01 ; 1.000119e+00 ; 6.560885e-01 ];

%-- Image #10:
omc_10 = [ -1.238911e+00 ; -1.736573e+00 ; 4.686593e-01 ];
Tc_10  = [ -2.401115e+02 ; -4.757064e+02 ; 1.501148e+03 ];
omc_error_10 = [ 6.464208e-04 ; 5.032781e-04 ; 7.880815e-04 ];
Tc_error_10  = [ 8.579411e-01 ; 1.102950e+00 ; 7.597893e-01 ];

%-- Image #11:
omc_11 = [ 2.045547e+00 ; 1.821039e+00 ; 9.134653e-01 ];
Tc_11  = [ 4.342569e+01 ; -2.791300e+02 ; 1.400682e+03 ];
omc_error_11 = [ 7.300562e-04 ; 4.517633e-04 ; 1.086176e-03 ];
Tc_error_11  = [ 8.037529e-01 ; 1.020076e+00 ; 7.902147e-01 ];

%-- Image #12:
omc_12 = [ -2.217867e+00 ; -2.113683e+00 ; -6.748516e-01 ];
Tc_12  = [ -1.398525e+02 ; -3.095491e+02 ; 1.262559e+03 ];
omc_error_12 = [ 4.968666e-04 ; 7.538129e-04 ; 1.231618e-03 ];
Tc_error_12  = [ 7.224762e-01 ; 9.278742e-01 ; 7.249527e-01 ];

%-- Image #13:
omc_13 = [ -1.919190e+00 ; -1.973928e+00 ; -4.115173e-02 ];
Tc_13  = [ -3.070593e+02 ; -3.194330e+02 ; 1.389968e+03 ];
omc_error_13 = [ 6.493970e-04 ; 5.755149e-04 ; 1.117502e-03 ];
Tc_error_13  = [ 7.868334e-01 ; 1.027896e+00 ; 7.314331e-01 ];

%-- Image #14:
omc_14 = [ -1.534320e+00 ; -1.762667e+00 ; 4.674434e-01 ];
Tc_14  = [ -2.227875e+02 ; -4.016223e+02 ; 1.558119e+03 ];
omc_error_14 = [ 6.404842e-04 ; 4.808041e-04 ; 8.564780e-04 ];
Tc_error_14  = [ 8.834584e-01 ; 1.141715e+00 ; 7.259776e-01 ];

%-- Image #15:
omc_15 = [ -1.339542e+00 ; -1.602065e+00 ; 7.002368e-01 ];
Tc_15  = [ -2.068710e+02 ; -4.350937e+02 ; 1.697919e+03 ];
omc_error_15 = [ 6.675962e-04 ; 5.018969e-04 ; 7.652696e-04 ];
Tc_error_15  = [ 9.652102e-01 ; 1.243490e+00 ; 7.687838e-01 ];

%-- Image #16:
omc_16 = [ NaN ; NaN ; NaN ];
Tc_16  = [ NaN ; NaN ; NaN ];
omc_error_16 = [ NaN ; NaN ; NaN ];
Tc_error_16  = [ NaN ; NaN ; NaN ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ NaN ; NaN ; NaN ];
Tc_error_19  = [ NaN ; NaN ; NaN ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

%-- Image #21:
omc_21 = [ 1.829737e+00 ; 1.759497e+00 ; 2.388949e-01 ];
Tc_21  = [ -1.723467e+02 ; -4.213853e+02 ; 1.353900e+03 ];
omc_error_21 = [ 6.166623e-04 ; 4.507978e-04 ; 9.840137e-04 ];
Tc_error_21  = [ 7.743068e-01 ; 9.908259e-01 ; 6.880953e-01 ];

%-- Image #22:
omc_22 = [ 2.050805e+00 ; 1.952903e+00 ; -2.354095e-01 ];
Tc_22  = [ -2.576173e+02 ; -3.394641e+02 ; 1.463603e+03 ];
omc_error_22 = [ 6.151001e-04 ; 5.675113e-04 ; 1.169877e-03 ];
Tc_error_22  = [ 8.289509e-01 ; 1.071542e+00 ; 6.681592e-01 ];

%-- Image #23:
omc_23 = [ -2.032222e+00 ; -1.914701e+00 ; 7.381536e-01 ];
Tc_23  = [ -2.521045e+02 ; -2.952320e+02 ; 1.558153e+03 ];
omc_error_23 = [ 6.654852e-04 ; 4.319626e-04 ; 1.054245e-03 ];
Tc_error_23  = [ 8.765393e-01 ; 1.142300e+00 ; 6.418172e-01 ];

%-- Image #24:
omc_24 = [ -1.567614e+00 ; -1.761192e+00 ; 8.135849e-01 ];
Tc_24  = [ -2.585084e+02 ; -3.722939e+02 ; 1.458554e+03 ];
omc_error_24 = [ 6.612909e-04 ; 4.591072e-04 ; 8.588241e-04 ];
Tc_error_24  = [ 8.272241e-01 ; 1.076909e+00 ; 6.159605e-01 ];

%-- Image #25:
omc_25 = [ -1.299963e+00 ; -1.796747e+00 ; 9.360902e-01 ];
Tc_25  = [ -1.789089e+02 ; -4.325525e+02 ; 1.585385e+03 ];
omc_error_25 = [ 6.523511e-04 ; 5.062682e-04 ; 8.239207e-04 ];
Tc_error_25  = [ 9.019990e-01 ; 1.165009e+00 ; 6.757174e-01 ];

