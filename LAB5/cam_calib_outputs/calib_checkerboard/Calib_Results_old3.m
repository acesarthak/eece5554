% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 4391.607827593534239 ; 4387.912543672586253 ];

%-- Principal point:
cc = [ 1565.973854294587909 ; 2014.326360682110817 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.176515259857448 ; -0.935119887700635 ; -0.007731830397828 ; 0.001875128508266 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 6.115681276990229 ; 5.938006269589025 ];

%-- Principal point uncertainty:
cc_error = [ 7.648786422740883 ; 9.865253759153124 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.010507011289353 ; 0.077077686346618 ; 0.000919410375463 ; 0.000718867573352 ; 0.000000000000000 ];

%-- Image size:
nx = 3072;
ny = 4096;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 25;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.205254e+00 ; 2.125109e+00 ; 1.185996e+00 ];
Tc_1  = [ 1.496640e+02 ; -3.182998e+02 ; 1.549341e+03 ];
omc_error_1 = [ 1.957462e-03 ; 1.771625e-03 ; 2.974250e-03 ];
Tc_error_1  = [ 2.712949e+00 ; 3.502005e+00 ; 2.498420e+00 ];

%-- Image #2:
omc_2 = [ 1.496368e+00 ; 2.269502e+00 ; -9.737185e-01 ];
Tc_2  = [ -5.091592e+01 ; -3.381323e+02 ; 2.047829e+03 ];
omc_error_2 = [ 1.314631e-03 ; 2.412976e-03 ; 3.302401e-03 ];
Tc_error_2  = [ 3.555729e+00 ; 4.619747e+00 ; 2.514351e+00 ];

%-- Image #3:
omc_3 = [ -1.227809e+00 ; -1.636694e+00 ; 3.254911e-01 ];
Tc_3  = [ -2.735792e+02 ; -4.835764e+02 ; 1.627506e+03 ];
omc_error_3 = [ 2.026502e-03 ; 1.581578e-03 ; 2.315346e-03 ];
Tc_error_3  = [ 2.861517e+00 ; 3.674359e+00 ; 2.595560e+00 ];

%-- Image #4:
omc_4 = [ -9.850837e-01 ; -1.763865e+00 ; 4.311474e-01 ];
Tc_4  = [ -1.027530e+02 ; -4.238271e+02 ; 1.485640e+03 ];
omc_error_4 = [ 1.840216e-03 ; 1.672091e-03 ; 2.319255e-03 ];
Tc_error_4  = [ 2.596230e+00 ; 3.337803e+00 ; 2.273969e+00 ];

%-- Image #5:
omc_5 = [ -2.210277e+00 ; -2.162953e+00 ; 1.258905e-01 ];
Tc_5  = [ -2.857460e+02 ; -2.604527e+02 ; 1.274866e+03 ];
omc_error_5 = [ 2.209305e-03 ; 2.110733e-03 ; 4.640890e-03 ];
Tc_error_5  = [ 2.215949e+00 ; 2.888332e+00 ; 2.021277e+00 ];

%-- Image #6:
omc_6 = [ 1.999247e+00 ; 1.745162e+00 ; 1.246243e+00 ];
Tc_6  = [ -1.038627e+02 ; -2.595954e+02 ; 1.128981e+03 ];
omc_error_6 = [ 2.177042e-03 ; 1.383400e-03 ; 3.082111e-03 ];
Tc_error_6  = [ 1.987038e+00 ; 2.552679e+00 ; 2.093462e+00 ];

%-- Image #7:
omc_7 = [ -2.145995e+00 ; -2.021987e+00 ; -8.307611e-01 ];
Tc_7  = [ -2.348781e+02 ; -2.181101e+02 ; 1.115824e+03 ];
omc_error_7 = [ 1.498241e-03 ; 2.224326e-03 ; 3.287044e-03 ];
Tc_error_7  = [ 1.946918e+00 ; 2.552625e+00 ; 1.986431e+00 ];

%-- Image #8:
omc_8 = [ -1.741961e+00 ; -1.915381e+00 ; -1.531210e-01 ];
Tc_8  = [ -2.560291e+02 ; -4.125494e+02 ; 1.347995e+03 ];
omc_error_8 = [ 1.896840e-03 ; 1.739432e-03 ; 3.037742e-03 ];
Tc_error_8  = [ 2.370319e+00 ; 3.080577e+00 ; 2.272962e+00 ];

%-- Image #9:
omc_9 = [ -1.474408e+00 ; -1.829108e+00 ; 2.924588e-01 ];
Tc_9  = [ -2.177516e+02 ; -3.653428e+02 ; 1.362391e+03 ];
omc_error_9 = [ 1.850895e-03 ; 1.548112e-03 ; 2.629944e-03 ];
Tc_error_9  = [ 2.371942e+00 ; 3.072301e+00 ; 2.017020e+00 ];

%-- Image #10:
omc_10 = [ -1.238575e+00 ; -1.736536e+00 ; 4.681942e-01 ];
Tc_10  = [ -2.399319e+02 ; -4.764900e+02 ; 1.501300e+03 ];
omc_error_10 = [ 1.986366e-03 ; 1.547956e-03 ; 2.421258e-03 ];
Tc_error_10  = [ 2.641354e+00 ; 3.388181e+00 ; 2.335840e+00 ];

%-- Image #11:
omc_11 = [ 2.045907e+00 ; 1.821003e+00 ; 9.139639e-01 ];
Tc_11  = [ 4.359476e+01 ; -2.798824e+02 ; 1.400729e+03 ];
omc_error_11 = [ 2.245131e-03 ; 1.389715e-03 ; 3.336336e-03 ];
Tc_error_11  = [ 2.474109e+00 ; 3.133467e+00 ; 2.428459e+00 ];

%-- Image #12:
omc_12 = [ -2.217415e+00 ; -2.113153e+00 ; -6.751485e-01 ];
Tc_12  = [ -1.396695e+02 ; -3.101896e+02 ; 1.262791e+03 ];
omc_error_12 = [ 1.526886e-03 ; 2.316373e-03 ; 3.782356e-03 ];
Tc_error_12  = [ 2.224434e+00 ; 2.850746e+00 ; 2.228048e+00 ];

%-- Image #13:
omc_13 = [ -1.918975e+00 ; -1.973752e+00 ; -4.175248e-02 ];
Tc_13  = [ -3.068915e+02 ; -3.201283e+02 ; 1.390106e+03 ];
omc_error_13 = [ 1.994634e-03 ; 1.768217e-03 ; 3.433291e-03 ];
Tc_error_13  = [ 2.422204e+00 ; 3.157752e+00 ; 2.248443e+00 ];

%-- Image #14:
omc_14 = [ -1.533920e+00 ; -1.762570e+00 ; 4.670694e-01 ];
Tc_14  = [ -2.225999e+02 ; -4.024416e+02 ; 1.558366e+03 ];
omc_error_14 = [ 1.968341e-03 ; 1.478526e-03 ; 2.631578e-03 ];
Tc_error_14  = [ 2.719965e+00 ; 3.507509e+00 ; 2.232044e+00 ];

%-- Image #15:
omc_15 = [ -1.339201e+00 ; -1.602078e+00 ; 6.998156e-01 ];
Tc_15  = [ -2.066723e+02 ; -4.359616e+02 ; 1.698250e+03 ];
omc_error_15 = [ 2.051939e-03 ; 1.543402e-03 ; 2.351501e-03 ];
Tc_error_15  = [ 2.971782e+00 ; 3.820309e+00 ; 2.363727e+00 ];

%-- Image #16:
omc_16 = [ NaN ; NaN ; NaN ];
Tc_16  = [ NaN ; NaN ; NaN ];
omc_error_16 = [ NaN ; NaN ; NaN ];
Tc_error_16  = [ NaN ; NaN ; NaN ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ NaN ; NaN ; NaN ];
Tc_error_19  = [ NaN ; NaN ; NaN ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

%-- Image #21:
omc_21 = [ 1.830141e+00 ; 1.759566e+00 ; 2.391181e-01 ];
Tc_21  = [ -1.722095e+02 ; -4.220612e+02 ; 1.354104e+03 ];
omc_error_21 = [ 1.895591e-03 ; 1.388197e-03 ; 3.024059e-03 ];
Tc_error_21  = [ 2.383880e+00 ; 3.043926e+00 ; 2.115469e+00 ];

%-- Image #22:
omc_22 = [ 2.050987e+00 ; 1.953113e+00 ; -2.350466e-01 ];
Tc_22  = [ -2.574362e+02 ; -3.402316e+02 ; 1.463775e+03 ];
omc_error_22 = [ 1.890062e-03 ; 1.745419e-03 ; 3.596778e-03 ];
Tc_error_22  = [ 2.551910e+00 ; 3.291804e+00 ; 2.054155e+00 ];

%-- Image #23:
omc_23 = [ -2.031957e+00 ; -1.914857e+00 ; 7.375576e-01 ];
Tc_23  = [ -2.519303e+02 ; -2.960571e+02 ; 1.558355e+03 ];
omc_error_23 = [ 2.045960e-03 ; 1.327799e-03 ; 3.240012e-03 ];
Tc_error_23  = [ 2.698602e+00 ; 3.509155e+00 ; 1.973206e+00 ];

%-- Image #24:
omc_24 = [ -1.567213e+00 ; -1.761145e+00 ; 8.131325e-01 ];
Tc_24  = [ -2.583444e+02 ; -3.730380e+02 ; 1.458673e+03 ];
omc_error_24 = [ 2.032745e-03 ; 1.411585e-03 ; 2.638776e-03 ];
Tc_error_24  = [ 2.546731e+00 ; 3.308090e+00 ; 1.893774e+00 ];

%-- Image #25:
omc_25 = [ -1.299578e+00 ; -1.796879e+00 ; 9.358010e-01 ];
Tc_25  = [ -1.786754e+02 ; -4.333803e+02 ; 1.585655e+03 ];
omc_error_25 = [ 2.005640e-03 ; 1.556617e-03 ; 2.531446e-03 ];
Tc_error_25  = [ 2.777233e+00 ; 3.579057e+00 ; 2.077563e+00 ];

