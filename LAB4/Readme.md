# LAB4

Used Day 2/4 extra days.

## Perform the following steps to build your workspace

* `catkin_make`
  
## To collect imu_data and gps data (remember to source the workspace) and use instructions from previous labs to run their drivers.

### Record seperate rosbags or a single rosbag for all topics

* `cd src/data/bag_files`
* `rosbag record /imu_data -O name_of_your_file.bag`
* `rosbag record /magnetometer_data -O name_of_your_file.bag`
* `rosbag record /gps_data -O name_of_your_file.bag`
  
### or

* `rosbag record -a`

### In a new terminal run following command to convert the rosbag to mat file

* `cd src/analysis`
* `python3 imu_data_to_mat.py`

## To analyze the data first go to root of workspace i.e. lab4 folder then run following commands

* `cd src/analysis`

### Open Matlab and run file Navigation_analysis.mlx
