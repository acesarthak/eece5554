import rospy
import serial
from math import sin, pi
from std_msgs.msg import Float64
from nav_msgs.msg import Odometry
import utm
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from tf.transformations import quaternion_from_euler
import numpy as np


if __name__ == '__main__':
    rospy.init_node('imu_sensor')
    # serial_port = rospy.get_param('~port', '/dev/ttyUSB0')

    serial_port = rospy.get_param('~port', '/dev/pts/4')

    serial_baud = rospy.get_param('~baudrate', 115200)

    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.logdebug("Using imu sensor on port " +
                   serial_port+" at "+str(serial_baud))

    rospy.sleep(0.2)
    line = port.readline()

    imu_pub = rospy.Publisher('/imu_data', Imu, queue_size=1)
    mag_pub = rospy.Publisher('/magnetometer_data',
                              MagneticField, queue_size=1)

    rospy.logdebug("Initialization complete")
    iteration = 0
    rospy.loginfo("collecting imu data")

    try:
        while not rospy.is_shutdown():
            line = port.readline()

            if line == '':
                rospy.logwarn("No data")
            else:
                # print(line)

                l1 = (line.decode()).split(',')

                print(l1)

                q = quaternion_from_euler(np.deg2rad(
                    float(l1[3])), np.deg2rad(float(l1[2])), np.deg2rad(float(l1[1])))

                imu_data = Imu()
                imu_data.header.frame_id = "imu_data"
                imu_data.header.stamp = rospy.Time.now()
                imu_data.header.seq = iteration
                imu_data.orientation.x = q[0]
                imu_data.orientation.y = q[1]
                imu_data.orientation.z = q[2]
                imu_data.orientation.w = q[3]
                imu_data.linear_acceleration.x = float(l1[7])
                imu_data.linear_acceleration.y = float(l1[8])
                imu_data.linear_acceleration.z = float(l1[9])
                imu_data.angular_velocity.x = float(l1[10])
                imu_data.angular_velocity.y = float(l1[11])
                imu_data.angular_velocity.z = float((l1[12].split('*'))[0])

                mag_data = MagneticField()
                mag_data.header.frame_id = "magnetometer_data"
                mag_data.header.stamp = rospy.Time.now()
                mag_data.header.seq = iteration
                mag_data.magnetic_field.x = float(l1[4])/10000
                mag_data.magnetic_field.y = float(l1[5])/10000
                mag_data.magnetic_field.z = float(l1[6])/10000

                iteration += 1
                imu_pub.publish(imu_data)
                mag_pub.publish(mag_data)

                # print(line)
            # rospy.sleep(sleep_time)

    except rospy.ROSInterruptException:
        port.close()

    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down imu node...")
