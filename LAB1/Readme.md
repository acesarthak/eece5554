# LAB1

## Perform the following steps to build your workspace

* `catkin_make`
  
## To collect gps_data (remember to source the workspace)

### Record rosbag

* `cd src/data/bag_files`
* `rosbag record /gps_data -O name_of_your_file.bag`

### In a new terminal run following command to start the data driver

* `rosrun gps_driver gps_data.py`

## To analyze the data first go to root of workspace i.e. lab1 folder then run following commands

* `cd src/analysis_scripts`
* `python3 gps_data_analyze.py name_of_stationary_data_file.bag name_of_moving_data_file.bag actual_latitude_fro stationary_data_in_degrees actual_longitude_for_stationary_data_in_degrees`
* `For instance: python3 gps_data_analyze.py stat.bag move.bag 41.7725 -65.8876` 
  
### You can also run this file without any arguments and the default arguments will be used.

  
