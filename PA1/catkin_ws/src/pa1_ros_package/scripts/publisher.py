#!/usr/bin/env python

import rospy
from math import fabs
from std_msgs.msg import Float64
from geometry_msgs.msg import Pose
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState

pub_to_topic = rospy.Publisher(
    '/float_pub_topic', Float64, queue_size=1)


def main():
    rospy.init_node('publisher', anonymous=True)
    val = 0
    RATE = 60

    rate = rospy.Rate(RATE)
    del RATE
    while not rospy.is_shutdown():
        pub_to_topic.publish(float(val))
        rospy.loginfo(val)
        val += 1
        if val > 1000:
            val = 0
        rate.sleep()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
