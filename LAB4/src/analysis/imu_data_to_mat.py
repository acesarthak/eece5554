import rosbag
from scipy.io import savemat
import numpy as np

rpy = [[], [], []]
angular_velocity = [[], [], []]
linear_acc = [[], [], []]
magnetic_field = [[], [], []]
lat_long_alt = [[], [], []]
utm = [[], []]

for i in range(0, 2):
    bag = rosbag.Bag(f'../data/bag_files/second_{i}.bag', 'r')
    for topic, msg, t in bag.read_messages(topics=['/IMU']):
        yaw_pitch_roll = msg.header.frame_id.split(' ')
        yaw = float(yaw_pitch_roll[1])
        pitch = float(yaw_pitch_roll[2])
        roll = float(yaw_pitch_roll[3])

        rpy[0].append(roll)
        rpy[1].append(pitch)
        rpy[2].append(yaw)

        angular_velocity[0].append(msg.angular_velocity.x)
        angular_velocity[1].append(msg.angular_velocity.y)
        angular_velocity[2].append(msg.angular_velocity.z)

        linear_acc[0].append(msg.linear_acceleration.x)
        linear_acc[1].append(msg.linear_acceleration.y)
        linear_acc[2].append(msg.linear_acceleration.z)

    for topic, msg, t in bag.read_messages(topics=['/Mag']):
        magnetic_field[0].append(msg.magnetic_field.x)
        magnetic_field[1].append(msg.magnetic_field.y)
        magnetic_field[2].append(msg.magnetic_field.z)

    for topic, msg, t in bag.read_messages(topics=['/GPS']):
        lat_long_alt[0].append(msg.latitude)
        lat_long_alt[1].append(msg.longitude)
        lat_long_alt[2].append(msg.altitude)
        utm[0].append(msg.utmEasting)
        utm[1].append(msg.utmNorthing)

bag.close()

mdic = {"roll": rpy[0], "pitch": rpy[1], "yaw": rpy[2],
        "ang_vel_x": angular_velocity[0], "ang_vel_y": angular_velocity[1], "ang_vel_z": angular_velocity[2],
        "lin_acc_x": linear_acc[0], "lin_acc_y": linear_acc[1], "lin_acc_z": linear_acc[2],
        "mag_field_x": magnetic_field[0], "mag_field_y": magnetic_field[1], "mag_field_z": magnetic_field[2],
        "latitude": lat_long_alt[0], "longitude": lat_long_alt[1], "altitude": lat_long_alt[2],
        "utm_easting": utm[0], "utm_northing": utm[1]}
savemat("vehicle_correct_batch_second.mat", mdic)
