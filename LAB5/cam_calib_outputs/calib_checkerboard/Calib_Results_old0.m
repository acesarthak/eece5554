% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 4126.841032650469970 ; 4129.463687983020463 ];

%-- Principal point:
cc = [ 1565.691809471523811 ; 1973.874218372652422 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.167305853013418 ; -0.780304421294466 ; -0.009233726780185 ; -0.000180024695097 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 13.627597099453965 ; 13.225947289888667 ];

%-- Principal point uncertainty:
cc_error = [ 16.545767668931454 ; 22.434724761159451 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.022118504561305 ; 0.143544544009884 ; 0.002214656948659 ; 0.001641153995711 ; 0.000000000000000 ];

%-- Image size:
nx = 3072;
ny = 4096;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 25;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.203322e+00 ; 2.135442e+00 ; 1.167637e+00 ];
Tc_1  = [ 1.490098e+02 ; -3.050707e+02 ; 1.450602e+03 ];
omc_error_1 = [ 4.633089e-03 ; 4.164230e-03 ; 7.242101e-03 ];
Tc_error_1  = [ 5.848765e+00 ; 7.926237e+00 ; 5.595260e+00 ];

%-- Image #2:
omc_2 = [ 1.493527e+00 ; 2.265331e+00 ; -9.842343e-01 ];
Tc_2  = [ -4.918624e+01 ; -3.203959e+02 ; 1.948131e+03 ];
omc_error_2 = [ 3.204607e-03 ; 5.824887e-03 ; 7.850053e-03 ];
Tc_error_2  = [ 7.778454e+00 ; 1.062495e+01 ; 5.608034e+00 ];

%-- Image #3:
omc_3 = [ -1.234860e+00 ; -1.643882e+00 ; 3.287580e-01 ];
Tc_3  = [ -2.732763e+02 ; -4.681328e+02 ; 1.536297e+03 ];
omc_error_3 = [ 4.895143e-03 ; 3.691929e-03 ; 5.535455e-03 ];
Tc_error_3  = [ 6.220820e+00 ; 8.389680e+00 ; 5.845434e+00 ];

%-- Image #4:
omc_4 = [ -9.897620e-01 ; -1.763927e+00 ; 4.404668e-01 ];
Tc_4  = [ -1.024174e+02 ; -4.094581e+02 ; 1.400550e+03 ];
omc_error_4 = [ 4.439875e-03 ; 3.896790e-03 ; 5.552876e-03 ];
Tc_error_4  = [ 5.631268e+00 ; 7.606323e+00 ; 5.098792e+00 ];

%-- Image #5:
omc_5 = [ -2.214698e+00 ; -2.166251e+00 ; 1.335288e-01 ];
Tc_5  = [ -2.855999e+02 ; -2.488423e+02 ; 1.203447e+03 ];
omc_error_5 = [ 5.114081e-03 ; 4.942236e-03 ; 1.078409e-02 ];
Tc_error_5  = [ 4.812270e+00 ; 6.598878e+00 ; 4.549328e+00 ];

%-- Image #6:
omc_6 = [ 1.998458e+00 ; 1.750092e+00 ; 1.234747e+00 ];
Tc_6  = [ -1.040522e+02 ; -2.482370e+02 ; 1.047922e+03 ];
omc_error_6 = [ 5.136178e-03 ; 3.269054e-03 ; 7.431646e-03 ];
Tc_error_6  = [ 4.249884e+00 ; 5.725061e+00 ; 4.693409e+00 ];

%-- Image #7:
omc_7 = [ -2.152373e+00 ; -2.034119e+00 ; -8.106346e-01 ];
Tc_7  = [ -2.352090e+02 ; -2.090865e+02 ; 1.043992e+03 ];
omc_error_7 = [ 3.634037e-03 ; 5.347572e-03 ; 7.934483e-03 ];
Tc_error_7  = [ 4.192647e+00 ; 5.772217e+00 ; 4.476912e+00 ];

%-- Image #8:
omc_8 = [ -1.751239e+00 ; -1.923174e+00 ; -1.426476e-01 ];
Tc_8  = [ -2.560093e+02 ; -4.002763e+02 ; 1.271006e+03 ];
omc_error_8 = [ 4.575976e-03 ; 4.141921e-03 ; 7.310655e-03 ];
Tc_error_8  = [ 5.142955e+00 ; 7.031120e+00 ; 5.131105e+00 ];

%-- Image #9:
omc_9 = [ -1.484699e+00 ; -1.832836e+00 ; 2.996513e-01 ];
Tc_9  = [ -2.186492e+02 ; -3.522778e+02 ; 1.288796e+03 ];
omc_error_9 = [ 4.452878e-03 ; 3.630963e-03 ; 6.283178e-03 ];
Tc_error_9  = [ 5.160435e+00 ; 7.028671e+00 ; 4.527036e+00 ];

%-- Image #10:
omc_10 = [ -1.245993e+00 ; -1.740500e+00 ; 4.717546e-01 ];
Tc_10  = [ -2.406893e+02 ; -4.624057e+02 ; 1.419755e+03 ];
omc_error_10 = [ 4.793082e-03 ; 3.611438e-03 ; 5.783877e-03 ];
Tc_error_10  = [ 5.750486e+00 ; 7.749742e+00 ; 5.265125e+00 ];

%-- Image #11:
omc_11 = [ 2.047707e+00 ; 1.828486e+00 ; 8.955405e-01 ];
Tc_11  = [ 4.258062e+01 ; -2.670599e+02 ; 1.307752e+03 ];
omc_error_11 = [ 5.362185e-03 ; 3.260895e-03 ; 8.095176e-03 ];
Tc_error_11  = [ 5.326889e+00 ; 7.064253e+00 ; 5.455654e+00 ];

%-- Image #12:
omc_12 = [ -2.220313e+00 ; -2.119938e+00 ; -6.539543e-01 ];
Tc_12  = [ -1.401921e+02 ; -2.990147e+02 ; 1.182771e+03 ];
omc_error_12 = [ 3.704677e-03 ; 5.585925e-03 ; 9.154072e-03 ];
Tc_error_12  = [ 4.800652e+00 ; 6.446265e+00 ; 5.041565e+00 ];

%-- Image #13:
omc_13 = [ -1.928852e+00 ; -1.981213e+00 ; -3.430518e-02 ];
Tc_13  = [ -3.068563e+02 ; -3.070210e+02 ; 1.309672e+03 ];
omc_error_13 = [ 4.785439e-03 ; 4.217493e-03 ; 8.238914e-03 ];
Tc_error_13  = [ 5.250288e+00 ; 7.197567e+00 ; 5.065697e+00 ];

%-- Image #14:
omc_14 = [ -1.543319e+00 ; -1.766323e+00 ; 4.733314e-01 ];
Tc_14  = [ -2.233975e+02 ; -3.878227e+02 ; 1.476521e+03 ];
omc_error_14 = [ 4.729605e-03 ; 3.470459e-03 ; 6.284142e-03 ];
Tc_error_14  = [ 5.929103e+00 ; 8.034399e+00 ; 5.008541e+00 ];

%-- Image #15:
omc_15 = [ -1.347115e+00 ; -1.604332e+00 ; 7.040590e-01 ];
Tc_15  = [ -2.075375e+02 ; -4.203568e+02 ; 1.610364e+03 ];
omc_error_15 = [ 4.935606e-03 ; 3.613122e-03 ; 5.613786e-03 ];
Tc_error_15  = [ 6.486123e+00 ; 8.755652e+00 ; 5.302831e+00 ];

%-- Image #16:
omc_16 = [ NaN ; NaN ; NaN ];
Tc_16  = [ NaN ; NaN ; NaN ];
omc_error_16 = [ NaN ; NaN ; NaN ];
Tc_error_16  = [ NaN ; NaN ; NaN ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ NaN ; NaN ; NaN ];
Tc_error_19  = [ NaN ; NaN ; NaN ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

%-- Image #21:
omc_21 = [ 1.831339e+00 ; 1.764041e+00 ; 2.289566e-01 ];
Tc_21  = [ -1.718115e+02 ; -4.099286e+02 ; 1.277877e+03 ];
omc_error_21 = [ 4.602703e-03 ; 3.237459e-03 ; 7.221505e-03 ];
Tc_error_21  = [ 5.179578e+00 ; 6.945538e+00 ; 4.754125e+00 ];

%-- Image #22:
omc_22 = [ 2.054395e+00 ; 1.954161e+00 ; -2.392828e-01 ];
Tc_22  = [ -2.575490e+02 ; -3.276652e+02 ; 1.385669e+03 ];
omc_error_22 = [ 4.611002e-03 ; 4.185685e-03 ; 8.490614e-03 ];
Tc_error_22  = [ 5.557883e+00 ; 7.528842e+00 ; 4.623269e+00 ];

%-- Image #23:
omc_23 = [ -2.040648e+00 ; -1.918419e+00 ; 7.409547e-01 ];
Tc_23  = [ -2.533509e+02 ; -2.824418e+02 ; 1.480550e+03 ];
omc_error_23 = [ 4.856533e-03 ; 3.175602e-03 ; 7.713658e-03 ];
Tc_error_23  = [ 5.892921e+00 ; 8.053465e+00 ; 4.423114e+00 ];

%-- Image #24:
omc_24 = [ -1.578578e+00 ; -1.764895e+00 ; 8.172677e-01 ];
Tc_24  = [ -2.609735e+02 ; -3.596969e+02 ; 1.390021e+03 ];
omc_error_24 = [ 4.868385e-03 ; 3.309687e-03 ; 6.286109e-03 ];
Tc_error_24  = [ 5.580756e+00 ; 7.620804e+00 ; 4.258817e+00 ];

%-- Image #25:
omc_25 = [ -1.309575e+00 ; -1.800684e+00 ; 9.401970e-01 ];
Tc_25  = [ -1.808231e+02 ; -4.193011e+02 ; 1.509471e+03 ];
omc_error_25 = [ 4.802667e-03 ; 3.650572e-03 ; 6.057913e-03 ];
Tc_error_25  = [ 6.082111e+00 ; 8.233077e+00 ; 4.671089e+00 ];

