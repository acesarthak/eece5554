import rosbag
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from statistics import stdev, mean
import numpy as np
import sys
import utm
import math


def dist_from_mean(centroid, x_values, y_values):
    shortest_dist = []
    for i in range(len(x_values)):
        x = x_values[i]
        y = y_values[i]
        dist = math.sqrt((x - centroid[0])**2 + (y - centroid[1])**2)
        shortest_dist.append(dist)
        shortest_dist_square = [number**2 for number in shortest_dist]
    return mean(shortest_dist), min(shortest_dist), max(shortest_dist), mean(shortest_dist_square)


def shortest_distance(x1, y1, a, b, c):

    d = abs((a * x1 + b * y1 + c)) / (math.sqrt(a * a + b * b))
    return d


def absolute_error(slope, intercept, x_values, y_values):
    shortest_dist = []
    for i in range(len(x_values)):
        x = x_values[i]
        y = y_values[i]
        dist = shortest_distance(x, y, -1.0*slope, 1, -1.0*intercept)
        shortest_dist.append(dist)
        shortest_dist_square = [number**2 for number in shortest_dist]
    return mean(shortest_dist), min(shortest_dist), max(shortest_dist), mean(shortest_dist_square)


def best_line(utm_easting, utm_northing, breakpoints):

    # find line of best fit
    for i in range(len(breakpoints)-1):
        idx = breakpoints[i]
        if i == 3:
            val1 = np.array(utm_easting[idx:breakpoints[i+1]]
                            + utm_easting[0:breakpoints[0]])
            val2 = np.array(utm_northing[idx:breakpoints[i+1]]
                            + utm_northing[0:breakpoints[0]])

        else:
            val1 = np.array(utm_easting[idx:breakpoints[i+1]])
            val2 = np.array(utm_northing[idx:breakpoints[i+1]])

        a, b = np.polyfit(val1, val2, 1)

        mean_absolute_error, min_error, max_error, mean_squared_error = absolute_error(
            a, b, val1, val2)
        plt.plot(val1, a*val1 +
                 b, label="Best Fit Line" + str(i+1))

        print("Line " + str(i + 1))

        print("Max deviation from best fit line for any GPS data point: ", max_error)
        print("Min deviation from best fit line for any GPS data point: ", min_error)
        print("Mean Absolute error for GPS data points from best fit line: ",
              mean_absolute_error)
        print("Mean Squared error for GPS data points from best fit line: ",
              mean_squared_error)


def main(location, breakpoints, bag_stationary, bag_moving):

    print(location + " STATIONARY RTK_GPS DATA")
    utm_easting = []
    utm_northing = []
    altitude = []
    quality = []

    for topic, msg, t in bag_stationary.read_messages(topics=['/gps_data']):

        utm_easting.append(msg.utm_easting)
        utm_northing.append(msg.utm_northing)
        altitude.append(msg.altitude)
        quality.append(msg.quality)

    utm_easting = np.array(utm_easting)
    utm_northing = np.array(utm_northing)
    altitude = np.array(altitude)
    quality = np.array(quality)

    bag_stationary.close()

    mean_stationary = (mean(utm_easting), mean(utm_northing))
    # utm_val = utm.from_latlon(lat_actual_stationary, lon_actual_stationary)
    # actual_stationary = (utm_val[0], utm_val[1])
    mean_absolute_error, min_error, max_error, mean_squared_error = dist_from_mean(
        mean_stationary, utm_easting, utm_northing)

    print("Max deviation from mean for any GPS data point: ", max_error)
    print("Min deviation from mean for any GPS data point: ", min_error)
    print("Mean Absolute error for GPS data points from mean: ",
          mean_absolute_error)
    print("Mean Squared error for GPS data points from mean: ", mean_squared_error)
    print("Mean location of stationary data using GPS sensor: ", mean_stationary)
    # print("Actual location of stationary data from Google Maps: ", actual_stationary)
    # print("Error between mean gps sensor location and actual location: ",
    #       (mean_stationary[0] - actual_stationary[0], mean_stationary[1] - actual_stationary[1]))
    print("Standard deviation of stationary data: ",
          (stdev(utm_easting), stdev(utm_northing)))
    print("Altitude Range: ", (min(altitude), max(altitude)))
    print("Mean of Altitude: ", mean(altitude))

    # Stationary 2D plot

    cdict = {1: 'red', 4: 'blue', 5: 'green', 2: 'black', 3: 'black'}
    label = {1: 'GPS_Fix', 4: 'RTK_Fix',
             5: 'RTK_Float', 2: 'Undefined', 3: 'Undefined'}

    for g in np.unique(quality):
        ix = np.where(quality == g)
        plt.scatter(utm_easting[ix], utm_northing[ix], s=8,
                    c=cdict[g], label=label[g])
    plt.scatter(mean_stationary[0], mean_stationary[1],
                s=32, c='black', label="Mean of Data")

    plt.legend()

    plt.xlabel('utm_easting (m)')
    plt.ylabel('utm_northing (m)')
    plt.title('Stationary GPS Data in 2D ' + location)

    plt.show()

    # Stationary 3D plot
    ax = plt.axes(projection='3d')
    ax.set_xlabel('utm_easting (m)')
    ax.set_ylabel('utm_northing (m)')
    ax.set_zlabel('altitude (m)')
    ax.set_title('Stationary GPS Data in 3D ' + location)

    for g in np.unique(quality):
        ix = np.where(quality == g)
        # print(ix)
        ax.scatter3D(utm_easting[ix], utm_northing[ix], altitude[ix], s=8,
                     c=cdict[g], label=label[g])
    ax.legend()
    plt.show()

    print()
    print(location + " MOVING RTK_GPS DATA")
    utm_easting = []
    utm_northing = []
    altitude = []
    quality = []

    for topic, msg, t in bag_moving.read_messages(topics=['/gps_data']):
        utm_easting.append(msg.utm_easting)
        utm_northing.append(msg.utm_northing)
        altitude.append(msg.altitude)
        quality.append(msg.quality)

    bag_moving.close()

    # Moving 2D plot

    print("Altitude Range: ", (min(altitude), max(altitude)))
    print("Mean of Altitude: ", mean(altitude))
    best_line(utm_easting, utm_northing, breakpoints)

    utm_easting = np.array(utm_easting)
    utm_northing = np.array(utm_northing)
    altitude = np.array(altitude)
    quality = np.array(quality)

    for g in np.unique(quality):
        ix = np.where(quality == g)
        # print(ix)
        plt.scatter(utm_easting[ix], utm_northing[ix], s=8,
                    c=cdict[g], label=label[g])

    plt.xlabel('utm_easting (m)')
    plt.ylabel('utm_northing (m)')
    plt.title('Moving GPS Data in 2D with best fit line ' + location)
    plt.legend()
    plt.show()

    # Moving 3D plot
    ax = plt.axes(projection='3d')
    ax.set_xlabel('utm_easting (m)')
    ax.set_ylabel('utm_northing (m)')
    ax.set_zlabel('altitude (m)')
    ax.set_title('Moving GPS Data in 3D ' + location)

    for g in np.unique(quality):
        ix = np.where(quality == g)
        ax.scatter3D(utm_easting[ix], utm_northing[ix], altitude[ix], s=8,
                     c=cdict[g], label=label[g])
    ax.legend()
    plt.show()

    print(f"\n\n\n\n")


if len(sys.argv) == 5:
    bag_stationary_isec = rosbag.Bag(
        '../../data/bag_files/' + str(sys.argv[1]), 'r')
    bag_moving_isec = rosbag.Bag(
        '../../data/bag_files/' + str(sys.argv[2]), 'r')

    bag_stationary_tennis = rosbag.Bag(
        '../../data/bag_files/' + str(sys.argv[3]), 'r')
    bag_moving_tennis = rosbag.Bag(
        '../../data/bag_files/' + str(sys.argv[4]), 'r')

else:
    bag_stationary_isec = rosbag.Bag(
        '../../data/bag_files/isec_static_new.bag', 'r')
    bag_moving_isec = rosbag.Bag(
        '../../data/bag_files/isec_moving_new.bag', 'r')

    bag_stationary_tennis = rosbag.Bag(
        '../../data/bag_files/tennis_static_new.bag', 'r')
    bag_moving_tennis = rosbag.Bag(
        '../../data/bag_files/tennis_moving_new.bag', 'r')


location = {0: "ISEC", 1: "TENNIS"}
breakpoints_tennis = [245, 2290, 3251, 4939, 5419]
breakpoints_isec = [453, 1733, 2291, 3024, 3935]
main(location[0], breakpoints_isec, bag_stationary_isec, bag_moving_isec)
main(location[1], breakpoints_tennis, bag_stationary_tennis, bag_moving_tennis)
