# LAB2

## Perform the following steps to build your workspace

* `catkin_make`
  
## To collect gps_data (remember to source the workspace)

### Record rosbag

* `cd src/data/bag_files`
* `rosbag record /gps_data -O name_of_your_file.bag`

### In a new terminal run following command to start the data driver

* `rosrun gps_driver rtk_data.py`

## To analyze the data first go to root of workspace i.e. lab1 folder then run following commands

* `cd src/rtk_driver/analysis`
* `python3 gps_data_analyze.py name_of_stationary_data_file_1.bag name_of_moving_data_file_1 name_of_stationary_data_file_2.bag name_of_moving_data_file_2.bag`
* `For instance: python3 gps_data_analyze.py stat_1.bag move_1.bag stat_2.bag move_2.bag`
  
### You can also run this file without any arguments and the default arguments will be used.

  
