import rosbag
from scipy.io import savemat
import numpy as np

rpy = [[], [], []]
angular_velocity = [[], [], []]
linear_acc = [[], [], []]
magnetic_field = [[], [], []]
for i in range(15):
    # bag = rosbag.Bag('../../data/bag_files/test2.bag', 'r')

    bag = rosbag.Bag(f'../../data/bag_files/imu_{i}.bag', 'r')
    for topic, msg, t in bag.read_messages(topics=['/ImuLab3']):
        yaw_pitch_roll = msg.header.frame_id.split(' ')
        yaw = float(yaw_pitch_roll[1])
        pitch = float(yaw_pitch_roll[2])
        roll = float(yaw_pitch_roll[3])

        rpy[0].append(roll)
        rpy[1].append(pitch)
        rpy[2].append(yaw)

        angular_velocity[0].append(msg.angular_velocity.x)
        angular_velocity[1].append(msg.angular_velocity.y)
        angular_velocity[2].append(msg.angular_velocity.z)

        linear_acc[0].append(msg.linear_acceleration.x)
        linear_acc[1].append(msg.linear_acceleration.y)
        linear_acc[2].append(msg.linear_acceleration.z)

    for topic, msg, t in bag.read_messages(topics=['/MagLab3']):
        magnetic_field[0].append(msg.magnetic_field.x)
        magnetic_field[1].append(msg.magnetic_field.y)
        magnetic_field[2].append(msg.magnetic_field.z)

    bag.close()
    # print(len(angular_velocity[0]))
    print(len(rpy[0]))
    break


# mdic = {"x": angular_velocity[0], "y": angular_velocity[1], "z": angular_velocity[2]}
mdic = {"roll": rpy[0], "pitch": rpy[1], "yaw": rpy[2],
        "ang_vel_x": angular_velocity[0], "ang_vel_y": angular_velocity[1], "ang_vel_z": angular_velocity[2],
        "lin_acc_x": linear_acc[0], "lin_acc_y": linear_acc[1], "lin_acc_z": linear_acc[2],
        "mag_field_x": magnetic_field[0], "mag_field_y": magnetic_field[1], "mag_field_z": magnetic_field[2]}
# savemat("angular_velocity.mat", mdic)
savemat("10_min_imu.mat", mdic)
