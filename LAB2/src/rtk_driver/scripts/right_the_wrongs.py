import rosbag
import time
from rtk_driver.msg import gps

bag = rosbag.Bag(
    '/home/robosteps/EECE5554/repo/eece5554/LAB2/src/data/bag_files/isec_static.bag', 'r')

new_bag = rosbag.Bag(
    '/home/robosteps/EECE5554/repo/eece5554/LAB2/src/data/bag_files/tennis_static_new.bag', 'w')

i = 0

for topic, msg, t in bag.read_messages(topics=['/gps_data']):

    gps_data = gps()
    gps_data.header.frame_id = msg.header.frame_id
    gps_data.header.stamp = msg.header.stamp
    gps_data.header.seq = msg.header.seq
    gps_data.child_frame_id = msg.child_frame_id
    gps_data.latitude = msg.latitude
    gps_data.longitude = msg.longitude
    gps_data.altitude = msg.altitude
    gps_data.utm_easting = msg.utm_easting
    gps_data.utm_northing = msg.utm_northing
    gps_data.zone = msg.zone
    gps_data.letter = msg.letter
    gps_data.quality = msg.quality
    print(i)
    i += 1

    new_bag.write('/gps_data', gps_data)
    time.sleep(0.01)


bag.close()
new_bag.close()
