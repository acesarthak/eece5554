import rospy
import serial
from math import sin, pi
from std_msgs.msg import Float64
from nav_msgs.msg import Odometry
import utm
from gps_driver.msg import gps


if __name__ == '__main__':
    rospy.init_node('gps_sensor')
    serial_port = rospy.get_param('~port', '/dev/ttyUSB0')
    serial_baud = rospy.get_param('~baudrate', 4800)
    sampling_rate = rospy.get_param('~sampling_rate', 5.0)

    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.logdebug("Using gps sensor on port " +
                   serial_port+" at "+str(serial_baud))

    sampling_count = int(round(1/(sampling_rate*0.007913)))

    rospy.sleep(0.2)
    line = port.readline()

    gps_pub = rospy.Publisher('/gps_data', gps, queue_size=5)

    rospy.logdebug("Initialization complete")
    sleep_time = 1/sampling_rate - 0.025
    iteration = 0
    rospy.loginfo("collecting gps data")

    try:
        while not rospy.is_shutdown():
            line = port.readline()

            if line == '':
                rospy.logwarn("No data")
            else:

                l1 = (line.decode()).split(',')

                if l1[0] == '$GPGGA' and l1[2] != '':
                    latitude = l1[2] + " " + l1[3]
                    longitude = l1[4] + " " + l1[5]
                    altitude = l1[9] + " " + l1[10]

                    lat = l1[2].split('.')

                    lat_degrees = float(lat[0].rstrip(lat[0][-2:]))
                    lat_minutes = float(lat[0][-2:] + "." + lat[1])
                    net_latitude = lat_degrees + lat_minutes/60.0

                    long = l1[4].split('.')
                    long_degrees = float(long[0].rstrip(long[0][-2:]))
                    long_minutes = float(long[0][-2:] + "." + long[1])
                    net_longitude = long_degrees + long_minutes/60.0

                    if lat[0][0] == '0':
                        net_latitude = -1.0 * net_latitude
                    if long[0][0] == '0':
                        net_longitude = -1.0 * net_longitude
                    # print('lat_deg ', lat_degrees)
                    # print('lat_min ', lat_minutes)
                    # print('net_latitude ', net_latitude)
                    # print('long_deg ', long_degrees)
                    # print('long_min ', long_minutes)
                    # print('net_longitude ', net_longitude)

                    # # print(l1)
                    # print('lat ', latitude)
                    # print('long ', longitude)
                    # print('alt ', altitude)

                    lat = l1[2].split('.')
                    lat_deg = 0.0
                    lat_min = float(lat[0][-2:] + '.' + lat[1])
                    if len(lat[0]) == 5:
                        lat_deg = float(lat[0][0:3])

                    elif len(lat[0]) == 4:
                        lat_deg = float(lat[0][0:2])
                    elif len(lat[0]) == 3:
                        lat_deg = float(lat[0][0:1])

                    lat = 1.0*(lat_deg + lat_min/60.0)
                    if l1[3] == 'S':
                        lat = -1.0 * lat

                    #
                    lon = l1[4].split('.')
                    lon_deg = 0.0
                    lon_min = float(lon[0][-2:] + '.' + lon[1])
                    if len(lon[0]) == 5:
                        lon_deg = float(lon[0][0:3])
                    elif len(lon[0]) == 4:
                        lon_deg = float(lon[0][0:2])
                    elif len(lon[0]) == 3:
                        lon_deg = float(lon[0][0:1])

                    # print(lon[0].rstrip(lon[0][-2:]))
                    # print('lon_deg ', lon_deg)
                    # print('lon_min ', lon_min)
                    lon = 1.0*(lon_deg + lon_min/60.0)

                    if l1[5] == 'W':
                        lon = -1.0 * lon

                    utm_val = utm.from_latlon(lat, lon)
                    print(line)
                    print(" ")

                    gps_data = gps()
                    gps_data.header.frame_id = "gps_data"
                    gps_data.header.stamp = rospy.Time.now()
                    gps_data.header.seq = iteration
                    if l1[3] == 'S':
                        gps_data.latitude = -1*float(l1[2])
                    else:
                        gps_data.latitude = float(l1[2])
                    if l1[5] == 'W':
                        gps_data.longitude = -1*float(l1[4])
                    else:
                        gps_data.longitude = float(l1[4])
                    gps_data.altitude = float(l1[9])
                    gps_data.utm_easting = utm_val[0]
                    gps_data.utm_northing = utm_val[1]
                    gps_data.zone = utm_val[2]
                    gps_data.letter = utm_val[3]

                    iteration += 1
                    gps_pub.publish(gps_data)

                # print(line)
            rospy.sleep(sleep_time)

    except rospy.ROSInterruptException:
        port.close()

    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down gps node...")
