# PA1

## Perform the following steps to build your workspace

* `cd catkin_ws`
* `catkin_make`
  
### To run publisher and subscriber(remember to source the workspace)

* `rosrun pa1_ros_package publisher.py`
* `rosrun pa1_ros_package subscriber.py`

### To run service and client

* `rosrun pa1_ros_package add_two_ints_service.py`
* `rosrun pa1_ros_package add_two_ints_client.py a b`
  
#### Here a and b are the two integer values to be added.
